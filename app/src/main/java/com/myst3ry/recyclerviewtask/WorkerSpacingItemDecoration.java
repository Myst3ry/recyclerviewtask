package com.myst3ry.recyclerviewtask;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public final class WorkerSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private final int spacing;

    public WorkerSpacingItemDecoration(final int spacing) {
        this.spacing = spacing;
    }

    public WorkerSpacingItemDecoration(final Context context, final int spacing) {
        this(context.getResources().getDimensionPixelSize(spacing));
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                               @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = spacing;
        } else {
            outRect.top = 0;
        }

        outRect.left = spacing;
        outRect.right = spacing;
        outRect.bottom = spacing;
    }
}
