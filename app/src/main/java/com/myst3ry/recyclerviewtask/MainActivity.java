package com.myst3ry.recyclerviewtask;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;

import io.reactivex.disposables.CompositeDisposable;

public final class MainActivity extends AppCompatActivity {

    private static final int INITIAL_WORKERS_SIZE = 5;

    private WorkerAdapter adapter;
    private RecyclerView recyclerView;
    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        compositeDisposable = new CompositeDisposable();

        initAdapter();
        initRecyclerView();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            adapter.addWorker(WorkerGenerator.generateWorker());
            recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        addInitialWorkers(INITIAL_WORKERS_SIZE);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case R.id.shuffle_workers:
                shuffleCurrentWorkers();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initAdapter()  {
        adapter = new WorkerAdapter();
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.rv_workers);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        attachItemTouchHelper();
        addItemDecoration();
    }

    private void attachItemTouchHelper() {
        final WorkerItemTouchHelperCallback callback = new WorkerItemTouchHelperCallback(adapter);
        final ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(recyclerView);
    }

    private void addItemDecoration() {
        final WorkerSpacingItemDecoration decoration = new WorkerSpacingItemDecoration(this, R.dimen.item_spacing);
        recyclerView.addItemDecoration(decoration);
    }

    private void addInitialWorkers(final int size) {
        adapter.addWorkers(WorkerGenerator.generateWorkers(size));
    }

    private void shuffleCurrentWorkers() {
        compositeDisposable.add(adapter.shuffleWorkers());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}