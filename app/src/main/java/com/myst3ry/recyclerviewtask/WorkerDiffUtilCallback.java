package com.myst3ry.recyclerviewtask;

import android.support.v7.util.DiffUtil;

import java.util.List;

public final class WorkerDiffUtilCallback extends DiffUtil.Callback {

        private final List<Worker> oldWorkers;
        private final List<Worker> newWorkers;

        public WorkerDiffUtilCallback(final List<Worker> oldWorkers, final List<Worker> newWorkers) {
            this.oldWorkers = oldWorkers;
            this.newWorkers = newWorkers;
        }

        @Override
        public int getOldListSize() {
            return oldWorkers.size();
        }

        @Override
        public int getNewListSize() {
            return newWorkers.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldWorkers.get(oldItemPosition).getId() == newWorkers.get(newItemPosition).getId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldWorkers.get(oldItemPosition).getName().equals(newWorkers.get(newItemPosition).getName());
        }
    }

