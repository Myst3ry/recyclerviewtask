package com.myst3ry.recyclerviewtask;

public interface AdapterItemTouchHelper {

    void onItemMove(final int fromPosition, final int toPosition);

    void onItemDismiss(final int position);
}
