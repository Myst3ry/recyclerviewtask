package com.myst3ry.recyclerviewtask;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public final class WorkerAdapter extends RecyclerView.Adapter<WorkerAdapter.WorkerViewHolder>
        implements AdapterItemTouchHelper {

    private final List<Worker> workers;

    public WorkerAdapter() {
        this.workers = new ArrayList<>();
    }

    @NonNull
    @Override
    public WorkerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new WorkerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_worker, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull WorkerViewHolder holder, int position) {
        final Worker worker = workers.get(position);

        holder.photoImageView.setImageResource(worker.getPhoto());
        holder.nameTextView.setText(worker.getName());
        holder.ageTextView.setText(holder.itemView.getContext().getString(R.string.item_worker_age, worker.getAge()));
        holder.positionTextView.setText(worker.getPosition());
    }

    @Override
    public int getItemCount() {
        return workers.size();
    }

    public void addWorkers(final List<Worker> newWorkers) {
        workers.addAll(newWorkers);
    }

    public void clearWorkers() {
        workers.clear();
    }

    public void addWorker(final Worker worker) {
        workers.add(worker);
        notifyItemInserted(workers.size() - 1);
    }

    public Disposable shuffleWorkers() {
        final List<Worker> newWorkers = new ArrayList<>(workers);
        Collections.shuffle(newWorkers);

        return Single.fromCallable(() -> {
            final WorkerDiffUtilCallback diffUtilCallback = new WorkerDiffUtilCallback(workers, newWorkers);
            return DiffUtil.calculateDiff(diffUtilCallback, false);
        }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(diffResult -> {
                    clearWorkers();
                    addWorkers(newWorkers);
                    diffResult.dispatchUpdatesTo(this);
                });
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        final Worker worker = workers.get(fromPosition);
        workers.remove(worker);
        workers.add(toPosition, worker);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        workers.remove(position);
        notifyItemRemoved(position);
    }

    final class WorkerViewHolder extends RecyclerView.ViewHolder {

        final ImageView photoImageView;
        final TextView nameTextView;
        final TextView ageTextView;
        final TextView positionTextView;

        WorkerViewHolder(@NonNull View itemView) {
            super(itemView);
            photoImageView = itemView.findViewById(R.id.iv_worker_photo);
            nameTextView = itemView.findViewById(R.id.tv_worker_name);
            ageTextView = itemView.findViewById(R.id.tv_worker_age);
            positionTextView = itemView.findViewById(R.id.tv_worker_position);
        }
    }
}
